package com.example.shubhraj.firebase;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Shubhraj on 10-09-2017.
 */

public class FrontActivity extends AppCompatActivity
{
    private Button bookButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        bookButton = (Button) findViewById(R.id.book_button);

        bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ConnectivityManager cm =
                        (ConnectivityManager)getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected == false) {
                    // There are no active networks.
                    Toast.makeText(FrontActivity.this,"Please connect to the Internet",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent intent = new Intent(FrontActivity.this, BedBooking.class);
                    startActivity(intent);
                }
            }
        });
    }
}
